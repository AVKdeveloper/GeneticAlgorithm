#include "route.h"

list_t *list_create(size_t length) {
    if (length < 1) {
        fprintf(stderr, "Can not create list with length less than 1\n");
        exit(-1);
    }
    list_t *list_ptr = (list_t *)malloc(sizeof(list_t));
    list_ptr->length = length;
    list_ptr->head = (list_node_t *)malloc(sizeof(list_node_t));
    list_ptr->head->data = 1;
    list_node_t *current_node_ptr = list_ptr->head;
    for (int i = 1; i < length; ++i) {
        list_node_t *next_node_ptr = (list_node_t *)malloc(sizeof(list_node_t));
        next_node_ptr->next_node = NULL;
        next_node_ptr->data = i + 1;
        current_node_ptr->next_node = next_node_ptr;
        current_node_ptr = next_node_ptr;
    }
    return list_ptr;
}

void list_destroy(list_t *list_ptr) {
    if (list_ptr == NULL) {
        return;
    }
    list_node_t *current_node_ptr = list_ptr->head;
    for (int i = 1; i < list_ptr->length; ++i) {
        list_node_t *next_node_ptr = current_node_ptr->next_node;
        free(current_node_ptr);
        current_node_ptr = next_node_ptr;
    }
    if (current_node_ptr != NULL) {
        free(current_node_ptr);
    }
    free(list_ptr);
}

int list_remove(list_t *list_ptr, size_t i) {
    if (i >= list_ptr->length) {
        fprintf(stderr, "Invalid index of element in list\n");
        exit(-1);
    }
    list_ptr->length = list_ptr->length - 1;
    list_node_t *current_node_ptr = list_ptr->head;
    list_node_t *deleting_node_ptr = list_ptr->head;
    if (i == 0) {
        list_ptr->head = current_node_ptr->next_node;
    } else {
        for (int j = 0; j < i - 1; ++j) {
            current_node_ptr = current_node_ptr->next_node;
        }
        deleting_node_ptr = current_node_ptr->next_node;
        current_node_ptr->next_node = deleting_node_ptr->next_node;
    }
    int returning_data = deleting_node_ptr->data;
    free(deleting_node_ptr);
    return returning_data;
}


route_t *route_generate(size_t length, int get_random(void *),
                        void *random_params) {
    int *cities = (int *)malloc(length * sizeof(int));
    route_t *route_ptr = (route_t *)malloc(sizeof(route_t));
    route_ptr->cities = cities;
    route_ptr->length = length;
    route_ptr->value = -1;
    list_t *list_ptr = list_create(length);
    for (int i = 0; i < length; ++i) {
        int next_city = get_random(random_params) % (length - i);
        cities[i] = list_remove(list_ptr, next_city);
    }
    list_destroy(list_ptr);
    return route_ptr;
}

void route_destroy(route_t *route_ptr) {
    free(route_ptr->cities);
    free(route_ptr);
}

void route_print(route_t *route_ptr) {
    for (int i = 0; i < route_ptr->length; ++i) {
        printf("%d ", route_ptr->cities[i]);
    }
    printf("\n");
}

void route_print_to_file_full(FILE *file, route_t *route) {
    fprintf(file, "%d ", 0);
    for (int i = 0; i < route->length; ++i) {
        fprintf(file, "%d ", route->cities[i]);
    }
}

void route_swap(route_t *route_ptr, size_t idx1, size_t idx2) {
    if (idx1 >= route_ptr->length || idx2 >= route_ptr->length) {
        fprintf(stderr, "Indexes must be less than size in route_swap()\n");
        exit(-1);
    }
    int city = route_ptr->cities[idx1];
    route_ptr->cities[idx1] = route_ptr->cities[idx2];
    route_ptr->cities[idx2] = city;
}

int route_contains(route_t *route_ptr, int city) {
    // Return the first index of city in the route
    // If city is absent, function will return -1
    int result = -1;
    for (int i = 0; i < route_ptr->length; ++i) {
        if (route_ptr->cities[i] == city) {
            result = i;
            break;
        }
    }
    return result;
}

void route_mutation(route_t *route_ptr, int get_random(void *),
                    void *random_params) {
    size_t size = route_ptr->length;
    size_t idx1 = get_random(random_params) % size;
    size_t idx2 = get_random(random_params) % size;
    route_swap(route_ptr, idx1, idx2);
}

route_t *route_crossover(route_t *route_ptr1, route_t *route_ptr2,
                         int get_random(void *), void *random_params) {
    size_t length = route_ptr1->length;
    if (length != route_ptr2->length) {
        fprintf(stderr, "Impossible to cross two routes of different length\n");
        exit(-1);
    }
    int *cities = (int *)malloc(length * sizeof(int));
    route_t *route_ptr = (route_t *)malloc(sizeof(route_t));
    route_ptr->cities = cities;
    route_ptr->length = length;
    route_ptr->value = -1;
    size_t idx1 = get_random(random_params) % length;
    size_t idx2 = get_random(random_params) % length;
    if (idx2 < idx1) {
        size_t tmp = idx1;
        idx1 = idx2;
        idx2 = tmp;
    }
    memset((void *)cities, 0, idx1 * sizeof(int));
    memcpy(
        (void *)(&cities[idx1]),
        (void *)(&route_ptr1->cities[idx1]),
        (idx2 - idx1) * sizeof(int)
    );
    memset((void *)(&cities[idx2]), 0, (length - idx2) * sizeof(int));
    int j = -1;
    for (int i = 0; i < length; ++i) {
        if (route_contains(route_ptr, route_ptr2->cities[i]) == -1) {
            j++;
            if (j == idx1) {
                j = idx2;
            }
            cities[j] = route_ptr2->cities[i];
        }
    }
    return route_ptr;
}

int route_value(route_t *route_ptr, const graph_t *graph_ptr) {
    int result = 0;
    int weight = graph_weight(graph_ptr, 0, route_ptr->cities[0]);
    if (weight == -1) {
        fprintf(stderr, "Sizes disparity in route_value()\n");
        exit(-1);
    }
    result += weight;
    for (int i = 0; i < route_ptr->length - 1; ++i) {
        weight = graph_weight(
            graph_ptr,
            route_ptr->cities[i],
            route_ptr->cities[i + 1]
        );
        if (weight == -1) {
            fprintf(stderr, "Sizes disparity in route_value()\n");
            exit(-1);
        }
        result += weight;
    }
    weight = graph_weight(
        graph_ptr,
        route_ptr->cities[route_ptr->length - 1],
        0
    );
    if (weight == -1) {
        fprintf(stderr, "Sizes disparity in route_value()\n");
        exit(-1);
    }
    result += weight;
    if (result < 0) {
        fprintf(stderr, "Wrong result in route_value()\n");
        exit(-1);
    }
    route_ptr->value = result;
    return result;
}

int route_compare(const void *ptr1, const void *ptr2) {
    route_t **route_ptr1 = (route_t **)ptr1;
    route_t **route_ptr2 = (route_t **)ptr2;
    if ((*route_ptr1)->value == -1 || (*route_ptr2)->value == -1) {
        fprintf(stderr, "Uninitialized values for routes in route_compare()\n");
        exit(-1);
    }
    return (*route_ptr1)->value - (*route_ptr2)->value;
}
