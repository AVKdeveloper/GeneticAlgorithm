#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "graph.h"
#include "thread_pool.h"
#include "population.h"
#include "random_maker.h"

population_t *Master(const int number_slave_threads, const int population_size,
        const int max_useless_iterations, const graph_t *graph);

int main(int argc, char** argv) {
    if (argc != 6) {
        fprintf(stderr, "Wrong number of arguments!\n");
        exit(-1);
    }
    const int kSlaveThreads = atoi(argv[1]);
    if (kSlaveThreads < 1) {
        fprintf(stderr, "Wrong number of slave threads\n");
        exit(-1);
    }
    const int kPopulationSize = atoi(argv[2]);
    if (kPopulationSize < 1) {
        fprintf(stderr, "Wrong size of population!\n");
        exit(-1);
    }
    const int kMaxUselessIterations = atoi(argv[3]);
    if (kMaxUselessIterations < 1) {
        fprintf(stderr, "Wrong number of iterations without a progress!\n");
        exit(-1);
    }
    const char *kMode = argv[4];
    const char *kModeGenerate = "--generate";
    const char *kModeFile = "--file";
    const char *kFilePath = "./graph.txt";
    int graph_size = 0;
    graph_t *graph = NULL;
    srand(time(NULL));
    if (strcmp(kMode, kModeGenerate) == 0) {
        graph_size = atoi(argv[5]);
        if (graph_size < 2) {
            fprintf(stderr, "Wrong graph size!\n");
            exit(-1);
        }
        graph = graph_generate(graph_size, 100);
        graph_dump_file(graph, kFilePath);
    } else if (strcmp(kMode, kModeFile) == 0) {
        kFilePath = argv[5];
        graph = graph_read_file(kFilePath);
        graph_size = graph->n;
    } else {
        fprintf(stderr, "Invalid mode! Use --file or --generate\n");
        exit(-1);
    }
    struct timeval time_begin, time_end;
    gettimeofday(&time_begin, NULL);
    population_t *population = Master(kSlaveThreads, kPopulationSize,
        kMaxUselessIterations, graph);
    gettimeofday(&time_end, NULL);
    double working_time = (((double)(time_end.tv_sec - time_begin.tv_sec)) *
            1000000 + time_end.tv_usec - time_begin.tv_usec) / 1000000;
    graph_destroy(graph);
    FILE* file = fopen("stats.txt", "w");
    if (file == NULL) {
        printf("Problems with file!\n");
        exit(-1);
    }
    fprintf(file, "%d %d %d %d %d %fs %d\n", kSlaveThreads, kPopulationSize,
            kMaxUselessIterations, graph_size, population->total_iterations,
            working_time, population->best_value);
    route_print_to_file_full(file, population->individuals[0]);
    fclose(file);
    population_destroy(population);
    return 0;
}


int usual_random(void *random_params) {
    return rand();
}

population_t *Master(const int number_slave_threads, const int population_size,
        const int max_useless_iterations, const graph_t *graph) {
    population_t *population = population_create(graph, population_size,
            usual_random, NULL);
    // Size of arrays with random numbers
    const int kArraysSize = 100;
    // random maker considers this factor
    const int kSleepFactor = 4;
    // Fineness of tasks subdivision
    const int kDividingFactor = 2 * number_slave_threads;
    random_maker_t *random_maker = random_maker_create(kArraysSize,
            kSleepFactor * number_slave_threads);
    thread_pool_t *pool = thread_pool_create(number_slave_threads);
    random_params_t *random_params = random_params_create(number_slave_threads,
            random_maker->queue_ptr);
    random_params_add_map(random_params, pool->threads);
    /* First, we need to count fitness function of initial population */
    for (int i = 0; i <= population_size / kDividingFactor; ++i) {
        int from = i * kDividingFactor;
        int to = (i + 1) * kDividingFactor;
        if (to > population_size) {
            to = population_size;
        }
        queue_push(pool->tasks_queue, task_create(fitness_group,
                fitness_group_params_create(population, from, to)));
    }
    int iterations_to_end = 0;
    while (1) {
        iterations_to_end++;
        /* Crossover and fitness of new individuals */
        for (int i = 0; i <= population_size / kDividingFactor; ++i) {
            int from = i * kDividingFactor + population_size;
            int to = (i + 1) * kDividingFactor + population_size;
            if (to > population_size * 2) {
                to = population_size * 2;
            }
            queue_push(pool->tasks_queue, task_create(
                    crossover_and_fitness_group,
                    mutation_and_fitness_params_create(population, from, to,
                    get_random_number, random_params)));
        }
        thread_pool_local_synchronize(pool);
        /* Mutation and fitness of some of old individuals */
        for (int i = 0; i <= population_size / kDividingFactor; ++i) {
            if (i % 2 == 0) {
                continue;
            }
            int from = i * kDividingFactor;
            int to = (i + 1) * kDividingFactor;
            if (to > population_size) {
                to = population_size;
            }
            queue_push(pool->tasks_queue, task_create(
                    mutation_and_fitness,
                    mutation_and_fitness_params_create(population, from, to,
                    get_random_number, random_params)));
        }
        thread_pool_local_synchronize(pool);
        /* Finding best of population */
        queue_push(pool->tasks_queue, task_create(find_best,
                (void *)population));
        thread_pool_local_synchronize(pool);
        /* Finding other top half population */
        queue_push(pool->tasks_queue, task_create(ordered_selection,
                (void *)population));
        /* Check, if the end came (if the end is possible) */
        if (iterations_to_end >= max_useless_iterations) {
            thread_pool_global_synchronize(pool);
            iterations_to_end = population->no_progress_iterations;
            if (iterations_to_end >= max_useless_iterations) {
                break;
            }
        } else {
            thread_pool_local_synchronize(pool);
        }
    }
    thread_pool_destroy(pool);
    random_params_destroy(random_params);
    random_maker_destroy(random_maker);
    return population;
}
