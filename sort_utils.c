#include "sort_utils.h"

void *find_min(void *data, size_t quantity, size_t size,
               int (*compar)(const void *, const void *)) {
    void *result = data;
    for (int i = 1; i < quantity; ++i) {
        if (compar((void *)((char *)data + size * i), result) < 0) {
            result = (void *)((char *)data + size * i);
        }
    }
    return result;
}

void swap(void *array, int idx1, int idx2, size_t size) {
    memswap((void *)((char *)array + size * idx1),
            (void *)((char *)array + size * idx2), size);
}

void memswap(void *ptr1, void *ptr2, size_t size) {
    for (int i = 0; i < size; ++i) {
        char tmp = *((char *)ptr1 + i);
        *((char *)ptr1 + i) = *((char *)ptr2 + i);
        *((char *)ptr2 + i) = tmp;
    }
}

size_t partition(void *array, int from, int to, size_t size,
                 int (*compar)(const void *, const void *)) {
    void *pivot = (void *)((char *)array + (to - 1) * size);
    int i = from - 1;
    for (int j = from; j < to - 1; ++j) {
        if (compar((void *)((char *)array + size * j), pivot) <= 0) {
            i++;
            swap(array, i, j, size);
        }
    }
    swap(array, i + 1, to - 1, size);
    return i + 1;
}

void *find_order_statistic(void *array, size_t quantity, int k, size_t size,
                           int (*compar)(const void *, const void *)) {
    int left = 0;
    int right = quantity;
    while (1) {
        int mid = partition(array, left, right, size, compar);
        // printf("left = %d, right = %d, mid = %d, k = %d\n", left, right, mid, k);
        // sleep(1);
        if (mid == k) {
            return (void *)((char *)array + size * mid);
        } else if (k < mid) {
            right = mid;
        } else {
            // k -= mid + 1;
            left = mid + 1;
        }
    }
}
