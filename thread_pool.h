#ifndef GA_THREAD_POOL_H_
#define GA_THREAD_POOL_H_

#include <pthread.h>
#include <stdio.h>
#include "queue.h"
#include "cyclic_barrier.h"
#include "random_maker.h"

typedef struct thread_pool_t {
    size_t size;
    pthread_t *threads;
    queue_t *tasks_queue;
    cyclic_barrier_t *local_barrier;
    cyclic_barrier_t *global_barrier;
} thread_pool_t;

thread_pool_t *thread_pool_create(size_t size);
void thread_pool_destroy(thread_pool_t *pool);
void thread_pool_local_synchronize(thread_pool_t *pool);
void thread_pool_global_synchronize(thread_pool_t *pool);

void *worker(void *parameters);

typedef struct task_t {
    void *(*action)(void *);
    void *params;
} task_t;

task_t *task_create(void *(*action)(void *), void *params);
void task_destroy(task_t *task);

void *task_synchronize(void *params);

#endif  // GA_THREAD_POOL_H_
