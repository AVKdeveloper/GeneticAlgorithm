#ifndef GA_SORT_UTILS_H_
#define GA_SORT_UTILS_H_

#include <stdlib.h>

void *find_min(void *array, size_t quantity, size_t size,
               int (*compar)(const void *, const void *));
void swap(void *array, int idx1, int idx2, size_t size);
void memswap(void *ptr1, void *ptr2, size_t size);
size_t partition(void *array, int from, int to, size_t size,
                 int (*compar)(const void *, const void *));
void *find_order_statistic(void *array, size_t quantity, int k, size_t size,
                           int (*compar)(const void *, const void *));

#endif  // GA_SORT_UTILS_H_
