#ifndef GA_RANDOM_MAKER_H_
#define GA_RANDOM_MAKER_H_

#include <pthread.h>
#include <stdatomic.h>
#include <stdlib.h>
#include "queue.h"

typedef struct array_t {
    size_t size;
    size_t numbers_used;
    int *numbers;
} array_t;

array_t *array_create(size_t size);
void array_destroy(array_t *array_ptr);
int array_get_number(array_t *array_ptr);

typedef struct random_maker_t {
    size_t size;  // the size of creating arrays
    // if there are more than sleep_quantity arrays, maker sleeps
    size_t sleep_quantity;
    queue_t *queue_ptr;  // queue with arrays of random numbers
    atomic_int *is_end;
    pthread_t *thread;
} random_maker_t;

random_maker_t *random_maker_create(size_t size, size_t sleep_quantity);
void random_maker_destroy(random_maker_t *params);
void random_maker_signal_end(random_maker_t *params);

void *random_maker(void *random_maker_parameters);

//------------------------------------------------------------------------------

typedef struct random_params_t {
    size_t quantity;
    queue_t *random_queue;
    array_t **arrays;
    pthread_t *map;
} random_params_t;
random_params_t *random_params_create(size_t quantity, queue_t *random_queue);
void random_params_add_map(random_params_t *params, pthread_t *threads);
void random_params_destroy(random_params_t *params);
size_t get_thread_number(random_params_t *params);
int get_random_number(void *parameters);

#endif  // GA_RANDOM_MAKER_H_
