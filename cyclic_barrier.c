#include "cyclic_barrier.h"

cyclic_barrier_t *cyclic_barrier_create(size_t num_threads) {
    cyclic_barrier_t *barrier = (cyclic_barrier_t *)malloc(
            sizeof(cyclic_barrier_t));
    barrier->num_threads = num_threads;
    barrier->num_threads_before_barrier = 0;
    barrier->can_go_in = 0;
    barrier->can_go_out = 0;
    barrier->mutex_ptr = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(barrier->mutex_ptr, NULL);
    barrier->gate_in = (pthread_cond_t *)malloc(sizeof(pthread_cond_t));
    barrier->gate_out = (pthread_cond_t *)malloc(sizeof(pthread_cond_t));
    pthread_cond_init(barrier->gate_in, NULL);
    pthread_cond_init(barrier->gate_out, NULL);
    return barrier;
}

void cyclic_barrier_destroy(cyclic_barrier_t *barrier) {
    pthread_cond_destroy(barrier->gate_in);
    free(barrier->gate_in);
    pthread_cond_destroy(barrier->gate_out);
    free(barrier->gate_out);
    pthread_mutex_destroy(barrier->mutex_ptr);
    free(barrier->mutex_ptr);
    free(barrier);
}

void cyclic_barrier_pass(cyclic_barrier_t *barrier) {
    pthread_mutex_lock(barrier->mutex_ptr);
    barrier->num_threads_before_barrier++;
    if (barrier->num_threads_before_barrier == barrier->num_threads) {
        barrier->can_go_in = 1;
        barrier->can_go_out = 0;
        pthread_cond_broadcast(barrier->gate_in);
    } else {
        while (barrier->can_go_in != 1) {
            pthread_cond_wait(barrier->gate_in, barrier->mutex_ptr);
        }
    }
    barrier->num_threads_before_barrier--;
    if (barrier->num_threads_before_barrier == 0) {
        barrier->can_go_out = 1;
        barrier->can_go_in = 0;
        pthread_cond_broadcast(barrier->gate_out);
    } else {
        while (barrier->can_go_out != 1) {
            pthread_cond_wait(barrier->gate_out, barrier->mutex_ptr);
        }
    }
    pthread_mutex_unlock(barrier->mutex_ptr);
}
