#ifndef GA_ROUTE_H_
#define GA_ROUTE_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graph.h"

typedef struct list_node_t {
    int data;
    struct list_node_t *next_node;
} list_node_t;

typedef struct list_t {
    size_t length;
    list_node_t *head;
} list_t;

list_t *list_create(size_t length);
void list_destroy(list_t *list_ptr);
int list_remove(list_t *list_ptr, size_t i);


typedef struct route_t {
    size_t length;
    int *cities;
    int value;
} route_t;

route_t *route_generate(size_t length, int get_random(void *),
                        void *random_params);
void route_destroy(route_t *route_ptr);
void route_print(route_t *route_ptr);
void route_print_to_file_full(FILE *file, route_t *route);
void route_swap(route_t *route_ptr, size_t idx1, size_t idx2);
int route_contains(route_t *route_ptr, int city);
void route_mutation(route_t *route_ptr, int get_random(void *),
                    void *random_params);
route_t *route_crossover(route_t *route_ptr1, route_t *route_ptr2,
                         int get_random(void *), void *random_params);
int route_value(route_t *route_ptr, const graph_t *graph_ptr);

int route_compare(const void *ptr1, const void *ptr2);

#endif  // GA_ROUTE_H_
