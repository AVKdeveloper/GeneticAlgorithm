#include "population.h"

#include <limits.h>
#include <stdio.h>

population_t *population_create(const graph_t *graph, size_t quantity,
        int (*get_random)(void *), void *random_params) {
    population_t *population = (population_t *)malloc(sizeof(population_t));
    population->graph = graph;
    population->quantity = quantity;
    population->total_iterations = 0;
    population->best_value = INT_MAX;
    population->no_progress_iterations = 0;
    size_t extended_quantity = 2 * quantity;
    population->extended_quantity = extended_quantity;
    route_t **individuals = (route_t **)malloc(extended_quantity *
                                               sizeof(route_t *));
    for (int i = 0; i < quantity; ++i) {
        individuals[i] = route_generate(graph->n - 1, get_random,
                random_params);
    }
    for (int i = quantity; i < extended_quantity; ++i) {
        individuals[i] = NULL;
    }
    population->individuals = individuals;
    return population;
}

void population_destroy(population_t *population) {
    for (int i = 0; i < population->extended_quantity; ++i) {
        if (population->individuals[i] != NULL) {
            route_destroy(population->individuals[i]);
        }
    }
    free(population->individuals);
    free(population);
}

void population_mutation_one(population_t *population, size_t idx,
        int (*get_random)(void *), void *random_params) {
    route_t *route = population->individuals[idx];
    if (route == NULL) {
        fprintf(stderr, "Invalid route_ptr in population_mutation_one()\n");
        exit(-1);
    }
    route_mutation(route, get_random, random_params);
}

void population_mutation_group(population_t *population, size_t from, size_t to,
        int (*get_random)(void *), void *random_params) {
    if (from > to) {
        fprintf(stderr, "Invalid indexes from and to in mutation_group()\n");
        exit(-1);
    }
    if (to > population->extended_quantity) {
        fprintf(stderr, "Invalid index of individual in mutation_group()\n");
        exit(-1);
    }
    for (int i = from; i < to; ++i) {
        population_mutation_one(population, i, get_random, random_params);
    }
}

void population_crossover(population_t *population, size_t idx1, size_t idx2,
        size_t idx, int (*get_random)(void *), void *random_params) {
    if (idx1 >= population->quantity || idx2 >= population->quantity) {
        fprintf(stderr, "Invalid indexes of parents in crossover()\n");
        exit(-1);
    }
    if (idx >= population->extended_quantity || idx < population->quantity) {
        fprintf(stderr, "Invalid index of child in crossover()\n");
        exit(-1);
    }
    route_t *parent1 = population->individuals[idx1];
    route_t *parent2 = population->individuals[idx2];
    if (population->individuals[idx] != NULL) {
        route_destroy(population->individuals[idx]);
    }
    route_t *child = route_crossover(parent1, parent2, get_random,
                                     random_params);
    population->individuals[idx] = child;
}

void population_fitness_one(population_t *population, size_t idx) {
    route_t *route = population->individuals[idx];
    if (route == NULL) {
        fprintf(stderr, "Invalid route_ptr in fitness_group, i = %ld\n", idx);
        exit(-1);
    }
    route_value(route, population->graph);
}

void population_fitness_group(population_t *population, size_t from,
                              size_t to) {
    if (from > to) {
        fprintf(stderr, "Invalid indexes from and to in fitness_group()\n");
        exit(-1);
    }
    if (to > population->extended_quantity) {
        fprintf(stderr, "Invalid index of individual in fitness_group()\n");
        exit(-1);
    }
    for (int i = from; i < to; ++i) {
        population_fitness_one(population, i);
    }
}

void population_find_best(population_t *population) {
    route_t **best = (route_t **)find_min(population->individuals,
            population->extended_quantity, sizeof(route_t *), route_compare);
    route_t **first = &population->individuals[0];
    memswap(best, first, sizeof(route_t *));
    int best_value = population->individuals[0]->value;
    population->total_iterations++;
    if (best_value == population->best_value) {
        population->no_progress_iterations++;
    } else if (best_value < population->best_value) {
        population->best_value = best_value;
        population->no_progress_iterations = 0;
    } else {
        fprintf(stderr, "Degradation in best value!?\n");
        exit(-1);
    }
}

void population_ordered_selection(population_t *population) {
    // It's better, when population->quantity is even
    find_order_statistic(&population->individuals[1],
            population->extended_quantity - 1,
            (population->extended_quantity - 1) / 4 - 1,
            sizeof(route_t *), route_compare);
}

void population_sort_group(population_t *population, size_t from, size_t to) {
    if (from > to) {
        fprintf(stderr, "Invalid indexes from and to in sort_group()\n");
        exit(-1);
    }
    if (to > population->extended_quantity) {
        fprintf(stderr, "Invalid index of individual in sort_group()\n");
        exit(-1);
    }
    qsort((void *)&population->individuals[from], to - from, sizeof(route_t *),
          route_compare);
}

void population_print(population_t *population) {
    printf("\tPopulation:\n\t");
    for (int i = 0; i < population->extended_quantity; ++i) {
        if (population->individuals[i] != NULL) {
            for (int j = 0; j < population->individuals[i]->length; ++j) {
                printf("%d ", population->individuals[i]->cities[j]);
            }
            printf("Value=%d", population->individuals[i]->value);
            printf("\n\t");
        }
    }
    printf("\n");
}

//------------------------------------------------------------------------------

fitness_group_params_t *fitness_group_params_create(population_t *population,
        size_t from, size_t to) {
    fitness_group_params_t *params = (fitness_group_params_t *)malloc(
            sizeof(fitness_group_params_t));
    params->population = population;
    params->from = from;
    params->to = to;
    return params;
}

void *fitness_group(void *parameters) {
    fitness_group_params_t *params = (fitness_group_params_t *)parameters;
    population_fitness_group(params->population, params->from, params->to);
    free(params);
    return NULL;
}

crossover_and_fitness_params_t *crossover_and_fitness_params_create(
        population_t *population, size_t idx1, size_t idx2, size_t idx,
        int (*get_random)(void *), void *random_params) {
    crossover_and_fitness_params_t *params = (crossover_and_fitness_params_t *)
            malloc(sizeof(crossover_and_fitness_params_t));
    params->population = population;
    params->idx1 = idx1;
    params->idx2 = idx2;
    params->idx = idx;
    params->get_random = get_random;
    params->random_params = random_params;
    return params;
}

void *crossover_and_fitness(void *parameters) {
    crossover_and_fitness_params_t *params = (crossover_and_fitness_params_t *)
            parameters;
    population_crossover(params->population, params->idx1, params->idx2,
            params->idx, params->get_random, params->random_params);
    population_fitness_one(params->population, params->idx);
    free(params);
    return NULL;
}

void *crossover_and_fitness_group(void *parameters) {
    mutation_and_fitness_params_t *params = (mutation_and_fitness_params_t *)
            parameters;
    for (int i = params->from; i < params->to; ++i) {
        size_t idx1 = params->get_random(params->random_params) %
                params->population->quantity;
        size_t idx2 = params->get_random(params->random_params) %
                params->population->quantity;
        population_crossover(params->population, idx1, idx2, i,
                params->get_random, params->random_params);
        population_fitness_one(params->population, i);
    }
    free(params);
    return NULL;
}

mutation_and_fitness_params_t *mutation_and_fitness_params_create(
        population_t *population, size_t from, size_t to,
        int (*get_random)(void *), void *random_params) {
    mutation_and_fitness_params_t *params = (mutation_and_fitness_params_t *)
            malloc(sizeof(mutation_and_fitness_params_t));
    params->population = population;
    params->from = from;
    params->to = to;
    params->get_random = get_random;
    params->random_params = random_params;
    return params;
}

void *mutation_and_fitness(void *parameters) {
    mutation_and_fitness_params_t *params = (mutation_and_fitness_params_t *)
            parameters;
    population_mutation_group(params->population, params->from, params->to,
            params->get_random, params->random_params);
    population_fitness_group(params->population, params->from, params->to);
    free(params);
    return NULL;
}

void *find_best(void *parameters) {
    population_t *params = (population_t *)parameters;
    population_find_best(params);
    return NULL;
}

void *ordered_selection(void *parameters) {
    population_t *population = (population_t *)parameters;
    population_ordered_selection(population);
    int best_value = population->best_value;
    int worst_value = 0;
    double average_value = 0;
    for (int i = 0; i < population->quantity; ++i) {
        int current_value = population->individuals[i]->value;
        average_value += current_value;
        if (worst_value < current_value) {
            worst_value = current_value;
        }
    }
    average_value /= population->quantity;
    FILE* file = fopen("detailed_stats.txt", "a");
    fprintf(file, "%d %d %d %f\n", population->total_iterations,
            best_value, worst_value, average_value);
    fclose(file);
    return NULL;
}
