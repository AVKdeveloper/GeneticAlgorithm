#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "route.h"
#include "population.h"
#include "cyclic_barrier.h"
#include "sort_utils.h"
#include "random_maker.h"
#include "thread_pool.h"


void test1();
void test2();
void test3();
void test4();
void test5();
void test6();
void test7();
void test8();
void test9();
void test10();
void test11();

int main() {
    printf("Starting tests\n");
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    test10();
    test11();
    printf("All tests passed successfully!\n");
    return 0;
}

int get_random(void *random_params) {
    return rand();
}

void test1() {
    int route_size = 9;
    printf("Test for route_generate() and route_swap()\n");
    int (*my_func)(void *) = get_random;
    route_t *route_ptr = route_generate(route_size, my_func, NULL);
    assert(route_ptr->length == route_size);
    printf("\tGenerated route:\n\t");
    route_print(route_ptr);
    printf("\tAfter swapping 2th and 5th cities:\n\t");
    route_swap(route_ptr, 2, 5);
    route_print(route_ptr);
    route_destroy(route_ptr);
    printf("OK\n");
}

void test2() {
    int route_size = 9;
    printf("Test for route_mutation()\n");
    route_t *route_ptr = route_generate(route_size, get_random, NULL);
    assert(route_ptr->length == route_size);
    printf("\tGenerated route:\n\t");
    route_print(route_ptr);
    printf("\tAfter mutation:\n\t");
    route_mutation(route_ptr, get_random, NULL);
    route_print(route_ptr);
    route_destroy(route_ptr);
    printf("OK\n");
}

void test3() {
    int route_size = 9;
    printf("Test for route_contains()\n");
    route_t *route_ptr = route_generate(route_size, get_random, NULL);
    printf("\tGenerated route:\n\t");
    route_print(route_ptr);
    for (int i = 0; i <= route_size + 1; ++i) {
        printf("\troute contains %d at index %d\n", i,
               route_contains(route_ptr, i));
    }
    route_destroy(route_ptr);
    printf("OK\n");
}

void test4() {
    int route_size = 9;
    printf("Test for route_crossover()\n");
    for (int i = 0; i < 3; ++i) {
        route_t *route_ptr1 = route_generate(route_size, get_random, NULL);
        route_t *route_ptr2 = route_generate(route_size, get_random, NULL);
        printf("\tGenerated routes-parents:\n\t");
        route_print(route_ptr1);
        printf("\t");
        route_print(route_ptr2);
        printf("\tAfter crossover:\n\t");
        route_t *route_ptr = route_crossover(route_ptr1, route_ptr2, get_random, NULL);
        route_print(route_ptr);
        route_destroy(route_ptr);
        route_destroy(route_ptr1);
        route_destroy(route_ptr2);
    }
    printf("OK\n");
}

void test5() {
    int route_size = 3;
    printf("Test for route_value()\n");
    route_t *route_ptr = route_generate(route_size, get_random, NULL);
    printf("\tGenerated route:\n\t");
    route_print(route_ptr);
    graph_t *graph_ptr = graph_read_file("./graphs/graph1.txt");
    printf("\tValue for graphs/graph1.txt: %d\n",
           route_value(route_ptr, graph_ptr));
    graph_destroy(graph_ptr);
    route_destroy(route_ptr);
    printf("OK\n");
}

int CompareInt(const void *a, const void *b) {
    return *(int *)a - *(int *)b;
}

void test6() {
    printf("Test for sort_utils.h\n");
    int array1[10] = {3, 7, 4, 1, 2, 0, 8, 5, 9, 6};
    assert(*(int *)find_min(array1, 10, sizeof(int), CompareInt) == 0);
    swap(array1, 1, 6, sizeof(int));
    assert(array1[0] == 3);
    assert(array1[1] == 8);
    assert(array1[6] == 7);
    partition(array1, 0, 10, sizeof(int), CompareInt);
    assert(array1[6] == 6);
    int array2[10] = {3, 7, 4, 1, 2, 8, 0, 5, 9, 6};
    assert(partition(array2, 0, 10, sizeof(int), CompareInt) == 6);
    for (int i = 0; i < 10; ++i) {
        if (i == 6) {
            continue;
        } else if (i < 6) {
            assert(array2[i] < 6);
        } else {
            assert(array2[i] > 6);
        }
    }
    int array3[1] = {5};
    swap(array3, 0, 0, sizeof(int));
    assert(partition(array3, 0, 1, sizeof(int), CompareInt) == 0);
    int array4[5] = {0, 1, 2, 3, 4};
    assert(partition(array4, 0, 5, sizeof(int), CompareInt) == 4);
    int array5[10] = {3, 7, 4, 1, 2, 0, 8, 5, 9, 6};
    for (int i = 0; i < 10; ++i) {
        int order = i;
        assert(*(int *)find_order_statistic(array5, 10, order, sizeof(int),
                                            CompareInt) == i);
    }
    printf("\nOK\n");
}

void test7() {
    size_t quantity = 3;
    graph_t *graph_ptr = graph_read_file("./graphs/graph2.txt");
    printf("Test for population\n");
    population_t *population = population_create(graph_ptr, quantity,
            get_random, NULL);
    printf("\tGenerated population:\n");
    population_print(population);
    printf("\tAfter crossover 0, 1 -> 3 | 0, 2 -> 4 | 1, 2 -> 5:\n");
    population_crossover(population, 0, 1, 3, get_random, NULL);
    population_crossover(population, 0, 2, 4, get_random, NULL);
    population_crossover(population, 1, 2, 5, get_random, NULL);
    population_print(population);
    printf("\tAfter mutation of 0, 1, 2:\n");
    population_mutation_group(population, 0, 3, get_random, NULL);
    population_print(population);
    printf("\tAfter fitness function:\n");
    population_fitness_group(population, 0, 6);
    population_print(population);
    printf("\tCompare(population[1], population[3]) = %d\n",
           route_compare((void *)(&population->individuals[1]),
                         (void *)(&population->individuals[3])));
    printf("\tBest route:\n\t");
    route_print(*(route_t **)find_min(population->individuals, 6,
            sizeof(route_t *), route_compare));
    printf("\tAfter finding_best:\n");
    assert(population->best_value == INT_MAX);
    assert(population->no_progress_iterations == 0);
    population_find_best(population);
    printf("\tBest value: %d, iteration without progress: %d\n",
            population->best_value, population->no_progress_iterations);
    population_print(population);
    printf("\tAfter population_ordered_selection:\n");
    population_ordered_selection(population);
    assert(population->best_value == population->individuals[0]->value);
    population_print(population);
    population_find_best(population);
    assert(population->no_progress_iterations == 1);
    // printf("\tAfter 3th ordered statistics:\n");
    // find_order_statistic(population->individuals, 6, 3, sizeof(route_t *),
    //                      route_compare);
    // population_print(population);
    // printf("\tAfter sort:\n");
    // population_sort_group(population, 0, 6);
    // population_print(population);
    population_destroy(population);
    graph_destroy(graph_ptr);
    printf("OK\n");
}

typedef struct threads_params_t {
    int *ptr;
    size_t times;
    cyclic_barrier_t *barrier_ptr;
} threads_params_t;

void *increment_and_synchronize(void *parameters) {
    threads_params_t *params = (threads_params_t *)parameters;
    for (int i = 0; i < params->times; ++i) {
        (*params->ptr)++;
        // printf("\tset to %d\n", (*params->ptr));
        cyclic_barrier_pass(params->barrier_ptr);
    }
    return NULL;
}

void test8() {
    // Data race is possible, but there is great amount of time between
    // write and read at the one pointer
    printf("Test for cyclic_barrier\n");
    size_t num_threads = 3;
    size_t times = 10;
    cyclic_barrier_t *barrier = cyclic_barrier_create(num_threads + 1);
    int array[num_threads];
    threads_params_t params[num_threads];
    pthread_t threads[num_threads];
    for (int i = 0; i < num_threads; ++i) {
        array[i] = 0;
        params[i].ptr = &array[i];
        params[i].times = times;
        params[i].barrier_ptr = barrier;
        pthread_create(&threads[i], NULL, increment_and_synchronize,
                       &params[i]);
    }
    for (int i = 0; i < times; ++i) {
        usleep(100000);
        for (int j = 0; j < num_threads; ++j) {
            // printf("\titeration %d, index %d, value %d\n", i, j, array[j]);
            assert(array[j] == i + 1);
        }
        cyclic_barrier_pass(barrier);
    }
    for (int i = 0; i < num_threads; ++i) {
        pthread_join(threads[i], NULL);
    }
    cyclic_barrier_destroy(barrier);
    printf("OK\n");
}

void test9() {
    printf("Test for random_maker\n");
    size_t array_size = 100;
    size_t sleep_quantity = 10;
    random_maker_t *params = random_maker_create(array_size, sleep_quantity);
    int current_size = 0;
    while (1) {
        current_size = queue_len(params->queue_ptr);
        if (current_size == sleep_quantity) {
            break;
        } else {
            printf("\tCurrent sizeof queue = %d\n", current_size);
        }
    }
    array_t *array = (array_t *)queue_pop(params->queue_ptr);
    printf("\tFirst element of random_array = %d\n", array_get_number(array));
    usleep(10000);
    assert(array->size == array_size);
    current_size = queue_len(params->queue_ptr);
    printf("\tSizeof queue after all = %d\n", current_size);
    assert(current_size == sleep_quantity);
    array_destroy(array);
    random_maker_destroy(params);
    printf("OK\n");
}

void test10() {
    srand(10);
    size_t quantity = 3;
    graph_t *graph_ptr = graph_read_file("./graphs/graph2.txt");
    printf("Test for population {void *foo(void *)} functions\n");
    population_t *population = population_create(graph_ptr, quantity,
            get_random, NULL);
    printf("\tGenerated population:\n");
    population_print(population);
    printf("\tAfter fitness_group(from 0 to quantity):\n");
    fitness_group((void *)fitness_group_params_create(population, 0, quantity));
    population_print(population);
    printf("\tAfter crossover_and_fitness 0, 1 -> 3 | 0, 2 -> 4 | 1, 2 -> 5\n");
    crossover_and_fitness((void *)crossover_and_fitness_params_create(
            population, 0, 1, 3, get_random, NULL));
    crossover_and_fitness((void *)crossover_and_fitness_params_create(
            population, 0, 2, 4, get_random, NULL));
    crossover_and_fitness((void *)crossover_and_fitness_params_create(
            population, 1, 2, 5, get_random, NULL));
    population_print(population);
    printf("\tAfter mutation of 1 and 2:\n");
    mutation_and_fitness((void *)mutation_and_fitness_params_create(population,
            1, quantity, get_random, NULL));
    population_print(population);
    printf("\tAfter finding_best:\n");
    assert(population->best_value == INT_MAX);
    find_best((void *)population);
    assert(population->no_progress_iterations == 0);
    printf("\tBest value: %d, iteration without progress: %d\n",
            population->best_value, population->no_progress_iterations);
    population_print(population);
    printf("\tAfter population_ordered_selection:\n");
    ordered_selection((void *)population);
    assert(population->best_value == population->individuals[0]->value);
    population_print(population);
    find_best((void *)population);
    assert(population->no_progress_iterations == 1);
    population_destroy(population);
    graph_destroy(graph_ptr);
    printf("OK\n");
}

void *increment_by_ptr(void *parameters) {
    int *ptr = (int *)parameters;
    (*ptr)++;
    // printf("\tset to %d from %ld\n", (*ptr), pthread_self());
    return NULL;
}

void test11() {
    printf("Test for thread_pool\n");
    size_t num_threads = 3;
    size_t times = 10;
    int array[num_threads];
    for (int i = 0; i < num_threads; ++i) {
        array[i] = 0;
    }
    thread_pool_t *pool = thread_pool_create(num_threads);
    for (int i = 0; i < times; ++i) {
        for (int j = 0; j < num_threads; ++j) {
            queue_push(pool->tasks_queue, task_create(increment_by_ptr,
                    (void *)&array[j]));
        }
        if (i % 2 == 0) {
            thread_pool_global_synchronize(pool);
            for (int j = 0; j < num_threads; ++j) {
                printf("\tAfter %d iterations at idx %d value %d\n", i+1, j,
                        array[j]);
                assert(array[j] == i + 1);
            }
        } else {
            thread_pool_local_synchronize(pool);
        }
    }
    thread_pool_destroy(pool);
    printf("OK\n");
}
