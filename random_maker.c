#include "random_maker.h"

#include <stdio.h>

array_t *array_create(size_t size) {
    array_t *array_ptr = (array_t *)malloc(sizeof(array_t));
    array_ptr->size = size;
    array_ptr->numbers_used = 0;
    int *numbers = (int *)malloc(size * sizeof(int));
    for (int i = 0; i < size; ++i) {
        numbers[i] = rand();
    }
    array_ptr->numbers = numbers;
    return array_ptr;
}

void array_destroy(array_t *array_ptr) {
    free(array_ptr->numbers);
    free(array_ptr);
}

int array_get_number(array_t *array_ptr) {
    // Returns -1 in case all numbers were used
    if (array_ptr->numbers_used < array_ptr->size) {
        int number = array_ptr->numbers[array_ptr->numbers_used];
        array_ptr->numbers_used++;
        return number;
    }
    return -1;
}

random_maker_t *random_maker_create(size_t size, size_t sleep_quantity) {
    random_maker_t *params = (random_maker_t *)malloc(sizeof(random_maker_t));
    params->size = size;
    params->sleep_quantity = sleep_quantity;
    params->queue_ptr = queue_init(sleep_quantity);
    params->is_end = (atomic_int *)malloc(sizeof(atomic_int));
    atomic_init(params->is_end, 0);
    params->thread = (pthread_t *)malloc(sizeof(pthread_t));
    pthread_create(params->thread, NULL, random_maker, (void *)params);
    return params;
}

void random_maker_destroy(random_maker_t *params) {
    random_maker_signal_end(params);
    if (queue_len(params->queue_ptr) >= params->queue_ptr->max_size) {
        void *array_ptr = queue_pop(params->queue_ptr);
        array_destroy((array_t *)array_ptr);
    }
    pthread_join(*(params->thread), NULL);
    free(params->thread);
    while (queue_len(params->queue_ptr) > 0) {
        void *array_ptr = queue_pop(params->queue_ptr);
        array_destroy((array_t *)array_ptr);
    }
    queue_destroy(params->queue_ptr);
    free(params->is_end);
    free(params);
}

void random_maker_signal_end(random_maker_t *params) {
    atomic_store(params->is_end, 1);
}

void *random_maker(void *random_maker_parameters) {
    random_maker_t *params = (random_maker_t *)random_maker_parameters;
    while (1) {
        if (atomic_load(params->is_end) == 1) {
            return NULL;
        }
        queue_push(params->queue_ptr,
                (void *)array_create(params->size));
    }
}

//------------------------------------------------------------------------------

random_params_t *random_params_create(size_t quantity, queue_t *random_queue) {
    random_params_t *params = (random_params_t *)malloc(
            sizeof(random_params_t));
    params->quantity = quantity;
    params->random_queue = random_queue;
    params->arrays = (array_t **)malloc(quantity * sizeof(array_t *));
    params->map = (pthread_t *)malloc(quantity * sizeof(pthread_t));
    for (int i = 0; i < quantity; ++i) {
        params->arrays[i] = NULL;
        params->map[i] = 0;
    }
    return params;
}

void random_params_add_map(random_params_t *params, pthread_t *threads) {
    for (int i = 0; i < params->quantity; ++i) {
        params->map[i] = threads[i];
    }
}

void random_params_destroy(random_params_t *params) {
    free(params->map);
    for (int i = 0; i < params->quantity; ++i) {
        if (params->arrays[i] == NULL) {
            continue;
        }
        array_destroy(params->arrays[i]);
    }
    free(params->arrays);
    free(params);
}

size_t get_thread_number(random_params_t *params) {
    pthread_t self_id = pthread_self();
    for (size_t i = 0; i < params->quantity; ++i) {
        if (pthread_equal(self_id, params->map[i]) != 0) {
            return i;
        }
    }
    fprintf(stderr, "There is no array for thread %ld\n", self_id);
    exit(-1);
}

int get_random_number(void *parameters) {
    random_params_t *params = (random_params_t *)parameters;
    size_t idx = get_thread_number(params);
    while (params->arrays[idx] == NULL) {
        params->arrays[idx] = (array_t *)queue_pop(params->random_queue);
    }
    int random = array_get_number(params->arrays[idx]);
    if (random == -1) {
        array_destroy(params->arrays[idx]);
        params->arrays[idx] = NULL;
        return get_random_number(parameters);
    }
    return random;
}
