#ifndef GA_POPULATION_H_
#define GA_POPULATION_H_

#include <stdlib.h>
#include "graph.h"
#include "route.h"
#include "sort_utils.h"

typedef struct population_t {
    const graph_t *graph;
    size_t quantity;  // in ordinary population
    size_t extended_quantity;  // after crossover
    route_t **individuals;
    int total_iterations;
    int best_value;
    int no_progress_iterations;
} population_t;

population_t *population_create(const graph_t *graph, size_t quantity,
        int (*get_random)(void *), void * random_params);
void population_destroy(population_t *population);
void population_mutation_one(population_t *population, size_t idx,
        int (*get_random)(void *), void *random_params);
void population_mutation_group(population_t *population, size_t from, size_t to,
        int (*get_random)(void *), void *random_params);
void population_crossover(population_t *population, size_t idx1, size_t idx2,
        size_t idx, int (*get_random)(void *), void *random_params);
void population_fitness_one(population_t *population, size_t idx);
void population_fitness_group(population_t *population, size_t from, size_t to);
void population_find_best(population_t *population);
void population_ordered_selection(population_t *population);
void population_sort_group(population_t *population, size_t from, size_t to);
void population_print(population_t *population);

//------------------------------------------------------------------------------

typedef struct fitness_group_params_t {
    population_t *population;
    size_t from;
    size_t to;
} fitness_group_params_t;
fitness_group_params_t *fitness_group_params_create(population_t *population,
        size_t from, size_t to;);
void *fitness_group(void *parameters);

typedef struct crossover_and_fitness_params_t {
    population_t *population;
    size_t idx1;
    size_t idx2;
    size_t idx;
    int (*get_random)(void *);
    void *random_params;
} crossover_and_fitness_params_t;
crossover_and_fitness_params_t *crossover_and_fitness_params_create(
        population_t *population, size_t idx1, size_t idx2, size_t idx,
        int (*get_random)(void *), void *random_params);
void *crossover_and_fitness(void *parameters);

void *crossover_and_fitness_group(void *parameters);

typedef struct mutation_and_fitness_params_t {
    population_t *population;
    size_t from;
    size_t to;
    int (*get_random)(void *);
    void *random_params;
} mutation_and_fitness_params_t;
mutation_and_fitness_params_t *mutation_and_fitness_params_create(
        population_t *population, size_t from, size_t to,
        int (*get_random)(void *), void *random_params);
void *mutation_and_fitness(void *parameters);

void *find_best(void *parameters);
void *ordered_selection(void *parameters);

#endif  // GA_POPULATION_H_
