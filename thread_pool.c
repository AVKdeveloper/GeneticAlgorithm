#include "thread_pool.h"

#include <stdint.h>

thread_pool_t *thread_pool_create(size_t size) {
    thread_pool_t *pool = (thread_pool_t *)malloc(sizeof(thread_pool_t));
    pool->size = size;
    pool->tasks_queue = queue_init(SIZE_MAX);
    pool->local_barrier = cyclic_barrier_create(size);
    pool->global_barrier = cyclic_barrier_create(size + 1);
    pool->threads = (pthread_t *)malloc(size * sizeof(pthread_t));
    for (int i = 0; i < size; ++i) {
        pthread_create(&pool->threads[i], NULL, worker, (void *)pool);
    }
    return pool;
}

void thread_pool_destroy(thread_pool_t *pool) {
    for (int i = 0; i < pool->size; ++i) {
        task_t *end_task = task_create(NULL, NULL);
        queue_push(pool->tasks_queue, (void *)end_task);
    }
    for (int i = 0; i < pool->size; ++i) {
        pthread_join(pool->threads[i], NULL);
    }
    free(pool->threads);
    cyclic_barrier_destroy(pool->local_barrier);
    cyclic_barrier_destroy(pool->global_barrier);
    queue_destroy(pool->tasks_queue);
    free(pool);
}

void thread_pool_local_synchronize(thread_pool_t *pool) {
    for (int i = 0; i < pool->size; ++i) {
        task_t *task = task_create(task_synchronize,
                                   (void *)pool->local_barrier);
        queue_push(pool->tasks_queue, (void *)task);
    }
}

void thread_pool_global_synchronize(thread_pool_t *pool) {
    for (int i = 0; i < pool->size; ++i) {
        task_t *task = task_create(task_synchronize,
                                   (void *)pool->global_barrier);
        queue_push(pool->tasks_queue, (void *)task);
    }
    cyclic_barrier_pass(pool->global_barrier);
}

void *worker(void *parameters) {
    thread_pool_t *pool = (thread_pool_t *)parameters;
    while (1) {
        task_t *task = (task_t *)queue_pop(pool->tasks_queue);
        if (task->action == NULL) {
            task_destroy(task);
            break;
        }
        // void *result =
        task->action(task->params);
        task_destroy(task);
    }
    return NULL;
}

task_t *task_create(void *(*action)(void *), void *params) {
    task_t *task = (task_t *)malloc(sizeof(task_t));
    task->action = action;
    task->params = params;
    return task;
}

void task_destroy(task_t *task) {
    free(task);
}

void *task_synchronize(void *params) {
    cyclic_barrier_t *barrier = (cyclic_barrier_t *)params;
    cyclic_barrier_pass(barrier);
    return NULL;
}
