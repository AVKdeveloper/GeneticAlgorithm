#ifndef QUEUE_H
#define QUEUE_H

#include <pthread.h>
#include <stdlib.h>

#define MIN_QUEUE_SIZE 1

typedef struct queue_t {
	pthread_mutex_t lock;
	pthread_cond_t cv_put;
	pthread_cond_t cv_take;
	int head;
	int cap;
	int len;
	size_t max_size;

	void **data;
} queue_t;

queue_t *queue_init(size_t max_size);
void queue_destroy(queue_t *q);
void queue_push(queue_t *q, void *x);
void *queue_pop(queue_t *q);
int queue_len(queue_t *q);

#endif
