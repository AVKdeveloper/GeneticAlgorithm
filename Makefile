CFLAGS = -Wall -lpthread -Werror

all: run

run: main.o route.o graph.o population.o cyclic_barrier.o sort_utils.o queue.o random_maker.o thread_pool.o
	gcc main.o route.o graph.o population.o cyclic_barrier.o sort_utils.o queue.o random_maker.o thread_pool.o -o run $(CFLAGS)
test: unit_tests.o route.o graph.o population.o cyclic_barrier.o sort_utils.o queue.o random_maker.o thread_pool.o
	gcc unit_tests.o route.o graph.o population.o cyclic_barrier.o sort_utils.o queue.o random_maker.o thread_pool.o -o test $(CFLAGS)
main.o: main.c
	gcc main.c -c $(CFLAGS)
unit_tests.o: unit_tests.c
	gcc unit_tests.c -c $(CFLAGS)
route.o: route.c route.h
	gcc route.c -c $(CFLAGS)
graph.o: graph.c graph.h
	gcc graph.c -c $(CFLAGS)
population.o: population.c population.h
	gcc population.c -c $(CFLAGS)
cyclic_barrier.o: cyclic_barrier.c cyclic_barrier.h
	gcc cyclic_barrier.c -c $(CFLAGS)
sort_utils.o: sort_utils.c sort_utils.h
	gcc sort_utils.c -c $(CFLAGS)
queue.o: queue.c queue.h
	gcc queue.c -c $(CFLAGS)
random_maker.o: random_maker.c random_maker.h
	gcc random_maker.c -c $(CFLAGS)
thread_pool.o: thread_pool.c thread_pool.h
	gcc thread_pool.c -c $(CFLAGS)
clean:
	rm *.o test run
