#ifndef CYCLIC_BARRIER_H_
#define CYCLIC_BARRIER_H_

#include <pthread.h>
#include <stdlib.h>

typedef struct cyclic_barrier_t {
    size_t num_threads;
    size_t num_threads_before_barrier;
    int can_go_in;
    int can_go_out;
    pthread_mutex_t *mutex_ptr;
    pthread_cond_t *gate_in;
    pthread_cond_t *gate_out;
} cyclic_barrier_t;

cyclic_barrier_t *cyclic_barrier_create(size_t num_threads);
void cyclic_barrier_destroy(cyclic_barrier_t *barrier);
void cyclic_barrier_pass(cyclic_barrier_t *barrier);

#endif  // CYCLIC_BARRIER_H_
